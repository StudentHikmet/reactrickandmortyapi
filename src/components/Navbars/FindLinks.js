import React from "react";
import {gql, useQuery} from "@apollo/client";
import {Link, Redirect} from "react-router-dom";
import {Button} from "@material-ui/core";

export default function FindLinks(props) {
    let {loading, error, data} = useQuery(gql`
                query ($find: String)
                {
                    characters(filter: {name: $find}) {
                        results {
                            id,
                            name,
                        }
                    }
                    episodes(filter: {name: $find}) {
                        results {
                            id,
                            name,
                        }
                    }
                    locations(filter: {name: $find}) {
                        results {
                            id,
                            name,
                        }
                    }
                }
        `, {
            variables: {find: props.find}
        }
    );

    if (loading) return <p>Loading...</p>;
    if (error) return <Redirect from="episode/:id" to="/episodes"/>;

    return (
        <>
            <span>{props.find}</span>
        {
           data.episodes.results.slice(0,4).map( ({ id, name }) => (
               <Button component={Link} to={"/episode/"+props.id} >{name}</Button>
           ))
        }
        </>
    )
}