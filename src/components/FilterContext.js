import React, { Component } from 'react'

const FilterContext = React.createContext()

class FilterProvider extends Component {
    // Context state
    state = {
        filter: "",
    }

    // Method to update state
    setFilter = (filter) => {
        this.setState((prevState) => ({ filter }))
    }

    render() {
        const { children } = this.props
        const { filter } = this.state
        const { setFilter } = this

        return (
            <FilterContext.Provider
                value={{
                    filter,
                    setFilter,
                }}
            >
                {children}
            </FilterContext.Provider>
        )
    }
}

export default FilterProvider

export { FilterContext }