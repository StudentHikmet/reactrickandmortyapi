import React from "react";
import CardBody from "../../../components/Card/CardBody";
import Button from "../../../components/CustomButtons/Button";
import {Link} from "react-router-dom";
import Card from "../../../components/Card/Card";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../../assets/jss/material-dashboard-react/cardImagesStyles";

const useStyles = makeStyles(styles);

export default function EpisodeCard(props) {
    const classes = useStyles();
    return (
        <Card className={classes.card}>
            <CardBody>
                <h4>{props.name} - {props.episode}</h4>
                <p>{props.air_date}</p>
                <Button color="success" component={Link} to={"/episode/"+props.id}>More Info</Button>
            </CardBody>
        </Card>
    )
}