export default {
    noMargin: {
        margin: 0,
    },
    noMarginCenter: {
        margin: 0,
        textAlign: "center",
    }
}