import React from "react";
import FlexRow from "../../components/Flex/FlexRow";
import EpisodeList from "./inc/EpisodeList";
import PageControl from "../inc/PageControl";

import {FilterContext} from "../../components/FilterContext";

export default class Episodes extends React.Component {

    static contextType = FilterContext;

    constructor(props) {
        super(props);
        this.state = {page: 1, maxPage: 0};
        this.handleNewMaxPage = this.handleNewMaxPage.bind(this);
        this.setPage = this.setPage.bind(this);
    }

    handleNewMaxPage(value) {
        if (this.state.maxPage !== value) {
            this.setState(state => ({
                maxPage: value,
                page: 1
            }))
        }
    }

    setPage(value) {
        this.setState(state => ({
            page: value,
        }))
    }

    render() {
        const {filter} = this.context

        return (
            <>
                <PageControl container={this} min={1} max={this.state.maxPage} page={this.state.page}/>
                <FlexRow>
                    <EpisodeList page={this.state.page} setPage={this.setPage} MaxPageCallback={this.handleNewMaxPage}
                                 filter={filter}/>
                </FlexRow>
            </>
        )
    }
}