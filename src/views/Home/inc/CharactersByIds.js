import React from "react";
import {gql, useQuery} from "@apollo/client";
import CharacterCard from "../../Characters/inc/CharacterCard";

export default function CharactersByIds(props) {

    let { loading, error, data } = useQuery(gql`
                query ($ids: [ID!]!)
                {
                    charactersByIds(ids: $ids) {
                        id,
                        name,
                        status,
                        gender,
                        species,
                        image,
                        location {
                            name,
                        }
                    }
                }
        `, {
        variables: {ids: props.ids}
        }
    );

    if (loading) return <span>Loading...</span>
    if (error) return <span>Something went wrong...</span>

    return (
        data.charactersByIds.map( ({ id, name, status, gender, image, species, location }) => (
            <CharacterCard
                id={id}
                name={name}
                status={status}
                gender={gender}
                species={species}
                image={image}
                location={location}
            />
        ))
    )
}