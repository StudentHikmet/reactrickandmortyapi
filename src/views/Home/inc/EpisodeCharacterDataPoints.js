import React from "react";
import {gql, useQuery} from "@apollo/client";
import ChartistGraph from "react-chartist";
import CardHeader from "../../../components/Card/CardHeader";

import {
    CharacterDataPoint
} from "variables/charts.js";

export default function EpisodeCharacterDataPoint(props) {

    let {loading, error, data} = useQuery(gql`
                query ($page: Int)
                {
                    episodes(page: $page) {
                        info {
                            pages
                        }
                        results {
                            id,
                            episode
                            characters {
                                id,
                            }
                        }
                    }
                }
        `,{
            variables: { page: props.page }
        }
    );

    if (loading) return <CardHeader color="warning"><h2><b>Loading....</b></h2></CardHeader>;
    if (error) return <CardHeader color="danger"><h2><b>No Results...</b></h2></CardHeader>;

    props.MaxPageCallback(data.episodes.info.pages)

    CharacterDataPoint.data.labels = [];
    CharacterDataPoint.data.series[0] = [];

    for (let i = 0; i < data.episodes.results.length; i++) {
        CharacterDataPoint.data.labels.push(data.episodes.results[i].episode);
        CharacterDataPoint.data.series[0].push(data.episodes.results[i].characters.length);
        if (CharacterDataPoint.options.high < data.episodes.results[i].characters.length) {
            CharacterDataPoint.options.high = data.episodes.results[i].characters.length
        }
    }

    return (
        <CardHeader color="success">
            <ChartistGraph
                className="ct-chart"
                data={CharacterDataPoint.data}
                type="Line"
                options={CharacterDataPoint.options}
                listener={CharacterDataPoint.animation}
            />
        </CardHeader>
    )
}