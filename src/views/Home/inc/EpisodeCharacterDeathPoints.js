import React from "react";
import PageControl from "../../inc/PageControl";
import GridContainer from "../../../components/Grid/GridContainer";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import EpisodeCharacterDeathsDataPoints from "./EpisodeCharacterDeathsDataPoints";

export default class EpisodeCharacterPoints extends React.Component {

    constructor(props) {
        super(props);
        this.state = {page: 1, maxPage: 0};
        this.handleNewMaxPage = this.handleNewMaxPage.bind(this);
    }

    handleNewMaxPage(value) {
        if (this.state.maxPage !== value) {
            this.setState( state => ({
                maxPage: value,
                page: 1
            }))
        }
    }

    render() {
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card chart>
                        <EpisodeCharacterDeathsDataPoints page={this.state.page} MaxPageCallback={this.handleNewMaxPage} />
                        <CardBody>
                            <h4>Character deaths per episode.</h4>
                            <PageControl container={this} min={1} max={this.state.maxPage} page={this.state.page}/>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
    }
}