import React from "react";
import CountList from "./inc/CountList";
import EpisodeCharacterPoints from "./inc/EpisodeCharacterPoints";
import EpisodeCharacterDeathPoints from "./inc/EpisodeCharacterDeathPoints";
import CharactersByIds from "./inc/CharactersByIds";
import {Grid} from "@material-ui/core";

export default class Home extends React.Component {
    state = {count: 0, ids: []}

    UpdateCount = (count) => {
        if (count !== this.state.count) {

            const ids = [];

            while (ids.length < 3) {

                let N = Math.floor(Math.random() * count) + 1;

                while ( ids.includes(N)) N = Math.floor(Math.random() * count) + 1;

                ids.push(N);

            }

            this.setState(state => ({count: count, ids: ids}));

        }
    }


    render() {
        return (
            <>
                <CountList CountCallBack={this.UpdateCount}/>
                <Grid container justify="center">
                    {
                        this.state.count > 2 && <CharactersByIds ids={this.state.ids} count={this.state.count}/>
                    }
                </Grid>
                <EpisodeCharacterPoints/>
                <EpisodeCharacterDeathPoints/>
            </>
        )
    }

}