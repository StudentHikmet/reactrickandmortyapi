import React from "react";
import Loadingimg from "assets/img/loadingportal.gif";
// @material-ui/core components

import {gql, useQuery} from "@apollo/client";

// core components
import CharacterCard from "./CharacterCard";

export default function CharacterList(props) {

    let {loading, error, data} = useQuery(gql`
                query ($page: Int, $filter: String)
                {
                    characters(page: $page, filter: {name: $filter}) {
                        info {
                            pages
                        }
                        results {
                            id,
                            name,
                            gender,
                            image,
                            status,
                            species,
                            location {
                                name
                            },
                        }
                    }
                }
        `, {
            variables: {page: props.page, filter: props.filter}
        }
    );
    if (loading) return <img src={Loadingimg} alt="Loading..."/>;
    if (error) {
        props.MaxPageCallback(1);
        return <p>No Results</p>;
    }


    props.MaxPageCallback(data.characters.info.pages)

    return data.characters.results.map(({id, name, gender, image, status, species, location}) => (
        <CharacterCard
            id={id}
            name={name}
            gender={gender}
            image={image}
            status={status}
            species={species}
            location={location}
        />
    ));
}