import React from "react";

import {makeStyles} from "@material-ui/core/styles";

import styles from "assets/jss/material-dashboard-react/cardImagesStyles.js";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import Success from "../../../components/Typography/Success";
import Danger from "../../../components/Typography/Danger";
import Warning from "../../../components/Typography/Warning";
import Button from "../../../components/CustomButtons/Button";
import {Link} from "react-router-dom";

const useStyles = makeStyles(styles);

export default function CharacterCard(props) {
    const classes = useStyles();

    return (
        <Card className={classes.card}>
            <img
                className={classes.cardImgTop}
                data-src="holder.js/100px180/"
                alt="100%x180"
                style={{height: "60%", width: "100%", display: "block"}}
                src={props.image}
                data-holder-rendered="true"
            />
            <CardBody>
                <h4>{props.name}</h4>
                <p>{props.gender} - {props.species}</p>
                <p>
                    {
                        props.status === "Alive" ?
                            (
                                <Success>{props.status}</Success>
                            ) : props.status === "Dead" ?
                            (
                                <Danger>{props.status}</Danger>
                            ) :
                            (
                                <Warning>{props.status}</Warning>
                            )
                    }
                </p>
                {
                    props.location && <p>Location: {props.location.name}</p>
                }
                <Button color="success" component={Link} to={"/character/"+props.id}>More Info</Button>
            </CardBody>
        </Card>
    )
}