import React from "react";
import {gql, useQuery} from "@apollo/client";
import Loadingimg from "../../assets/img/loadingportal.gif";
import FlexRow from "../../components/Flex/FlexRow";
import {Divider, Grid} from "@material-ui/core";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import CharacterCard from "../Characters/inc/CharacterCard";

export default function Location(props) {
    let {loading, error, data} = useQuery(gql`
                query ($id: ID!)
                {
                    location(id: $id) {
                        id,
                        name,
                        type,
                        created,
                        residents{
                            id,
                            name,
                            gender,
                            image,
                            species,
                            status,
                        }
                        },
                    }
        `, {
            variables: {id: props.match.params.id}
        }
    );

    if (loading) return <FlexRow><img src={Loadingimg} alt="Loading..."/></FlexRow>;
    if (error) return <p>No Results...</p>;

    return (
        <>
            <Card>
                <CardBody>
                    <h1>{data.location.name}</h1>
                    <h5>Type: {data.location.type}</h5>
                </CardBody>
            </Card>
            <Divider />
                <h1 className="text-center">Residents</h1>
            <Divider />
            <Grid container justify="center">
                {
                    data.location.residents.map(({id, name, gender, image, status, species}) => (
                    <CharacterCard
                    id={id}
                    name={name}
                    gender={gender}
                    image={image}
                    status={status}
                    species={species}
                    />
                    ))
                }
            </Grid>
        </>
    )
}