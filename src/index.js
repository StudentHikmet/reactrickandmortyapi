import React from "react";
import ReactDOM from "react-dom";
import {createBrowserHistory} from "history";
import {Router, Route, Switch} from "react-router-dom"
import 'bootstrap/dist/css/bootstrap.css';
import FilterProvider from 'components/FilterContext';

// core components
import Main from "layouts/Main.js";
import "assets/css/material-dashboard-react.css?v=1.9.0";

import {ApolloProvider} from '@apollo/client';

import client from "variables/ApiClient";

const hist = createBrowserHistory();

ReactDOM.render(
    <ApolloProvider client={client}>
        <FilterProvider>
            <Router history={hist}>
                <Switch>
                    <Route component={Main}/>
                </Switch>
            </Router>
        </FilterProvider>
    </ApolloProvider>,

    document.getElementById("root")
);
